import Home from '../views/Home.vue';
import About from '../views/About.vue';
import NewNote from '../views/notes/Create.vue'
import tableOfNotes from '../views/notes/Table.vue'
import showTheNote from '../views/notes/Show.vue'
import EditNote from '../views/notes/Edit.vue'

export default {
    mode: 'history',
    linkActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'pages.about',
            component: About
        },
        {
            path: '/notes/create',
            name: 'notes.create',
            component: NewNote
        },
        {
            path: '/notes/table',
            name: 'notes.index',
            component: tableOfNotes
        },
        {
            path: '/notes/:noteSlug',
            name: 'notes.show',
            component: showTheNote
        },
        {
            path: '/notes/:noteSlug/edit',
            name: 'notes.edit',
            component: EditNote
        },
    ]
}