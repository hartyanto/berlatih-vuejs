<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Notes')->group(function () {
    Route::prefix('notes')->group(function () {
        // Route::post('create-new-post', 'Posts/PostController@store');
        Route::post('create-new-note', 'NoteController@store');
        Route::get('', 'NoteController@index');
        Route::get('{note}', 'NoteController@show')->name('notes.show');
        Route::patch('{note}', 'NoteController@update')->name('notes.update');
        Route::delete('{note}', 'NoteController@destroy')->name('notes.destroy');
    });

    // prefix adalah yang ada di url
    Route::prefix('subjects')->group(function () {
        Route::get('', 'SubjectController@index');
    });
});
